#!/bin/bash
if [ -e "org1" ]; then
   rm -rf org*
fi
if [ -e "./crypto-config.yaml" ]; then
   rm -rf ./crypto-config.yaml
fi
if [ -e "./configtx.yaml" ]; then
   rm -rf ./configtx.yaml
fi
if [ -e "./env.sh" ]; then
   rm -rf ./env.sh
fi
