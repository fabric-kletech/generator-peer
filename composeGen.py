#!/usr/bin/python3
import importlib
import sys
import os
from environment.conf import Configuration
imageTag = "1.4.7"
#Put "number1" space and "number" line jump 
def jumptab(number, number1):
    i = 0
    rslt = ""
    while (i < number):
        rslt = rslt + '\n'
        i += 1
    i = 0
    while (i < number1):
        rslt = rslt + "  "
        i += 1
    return (rslt)

def createChannelProfile(orgName, profileName):
    rslt = jumptab(0, 2) + profileName + ":"
    rslt += jumptab(1, 4) + "Consortium: SampleConsortium"
    rslt += jumptab(1, 4) + "<<: *ChannelDefaults"
    rslt += jumptab(1, 4) + "Application:"
    rslt += jumptab(1, 6) + "<<: *ApplicationDefaults"
    rslt += jumptab(1, 6) + "Organizations:"
    i = 2
    while (i < len(orgName)):
        rslt += jumptab(1, 8) + "- *" + orgName[i].capitalize()
        i += 2
    rslt += jumptab(1, 6) + "Capabilities:"
    rslt += jumptab(1, 8) + "<<: *ApplicationCapabilities"
    return (rslt)

#Part of configtx.yaml
def createProfiles(orgName, profileName, ordererName):
    rslt = jumptab(0, 2) + profileName + ":"
    rslt += jumptab(1, 4) + "<<: *ChannelDefaults"
    rslt += jumptab(1, 4) + "Orderer:"
    rslt += jumptab(1, 6) + "<<: *OrdererDefaults"
    rslt += jumptab(1, 6) + "Organizations:"
    rslt += jumptab(1, 8) + "- *" + ordererName
    rslt += jumptab(1, 6) + "Capabilities:"
    rslt += jumptab(1, 8) + "<<: *OrdererCapabilities"
    rslt += jumptab(1, 4) + "Consortiums:"
    rslt += jumptab(1, 6) + "SampleConsortium:"
    rslt += jumptab(1, 8) + "Organizations:"
    i = 2
    while (i < len(orgName)):
        rslt += jumptab(1, 10) + "- *" + orgName[i].capitalize()
        i += 2
    return (rslt)

#Part of configtx.yaml
def createOrganisations(name, opt, host):
    hostname = name.capitalize() + "MSP"
    if(name=="OrdererOrg"):
        rslt = jumptab(0, 2) + "- &" + name
        rslt = rslt + jumptab(1, 4) + "Name: " + name
    else:
        rslt = jumptab(0, 2) + "- &" + name.capitalize()
        rslt = rslt + jumptab(1, 4) + "Name: " + name.capitalize() + "MSP"
    if (opt == 1):
        rslt = rslt + jumptab(1, 4) + "ID: " + name.capitalize() + "MSP"
        rslt = rslt + jumptab(1, 4) + "MSPDir: crypto-config/peerOrganizations/" + name + "." + host + ".com/msp"
    else:
        rslt = rslt + jumptab(1, 4) + "ID: OrdererMSP"
        rslt = rslt + jumptab(1, 4) + "MSPDir: crypto-config/ordererOrganizations/" + host + ".com/msp"
    if(name=="OrdererOrg"):
        rslt = rslt + jumptab(1, 4) + "Policies:"
        rslt = rslt + jumptab(1, 6) + "Readers:"
        rslt = rslt + jumptab(1, 8) + "Type: Signature"
        rslt = rslt + jumptab(1, 8) + 'Rule: "OR(\'OrdererMSP.member\')"'
        rslt = rslt + jumptab(1, 6) + "Writers:"
        rslt = rslt + jumptab(1, 8) + "Type: Signature"
        rslt = rslt + jumptab(1, 8) + 'Rule: "OR(\'OrdererMSP.member\')"'
        rslt = rslt + jumptab(1, 6) + "Admins:"
        rslt = rslt + jumptab(1, 8) + "Type: Signature"
        rslt = rslt + jumptab(1, 8) + 'Rule: "OR(\'OrdererMSP.admin\')"'
    else:
        rslt = rslt + jumptab(1, 4) + "Policies:"
        rslt = rslt + jumptab(1, 6) + "Readers:"
        rslt = rslt + jumptab(1, 8) + "Type: Signature"
        rslt = rslt + jumptab(1, 8) + "Rule: \"OR(\'" + hostname + ".admin\', \'" + hostname + ".peer\', \'" + hostname + ".client\')\""
        rslt = rslt + jumptab(1, 6) + "Writers:"
        rslt = rslt + jumptab(1, 8) + "Type: Signature"
        rslt = rslt + jumptab(1, 8) + "Rule: \"OR(\'" + hostname + ".admin\', \'" + hostname + ".client\')\""
        rslt = rslt + jumptab(1, 6) + "Admins:"
        rslt = rslt + jumptab(1, 8) + "Type: Signature"
        rslt = rslt + jumptab(1, 8) + "Rule: \"OR(\'" + hostname + ".admin\')\""
    if (opt == 1):
        rslt = rslt + jumptab(1, 4) + "AnchorPeers:"
        rslt = rslt + jumptab(1, 6) + "- Host: peer0." + name + "." + host + ".com"
        rslt = rslt + jumptab(1, 6) + "  Port: 7051"
    return (rslt)

def createSoloOrderer(typeorderer, messageCount, absoluteMaxBytes, preferredMaxBytes, host):
    rslt = jumptab(2, 0) + "Orderer: &OrdererDefaults"
    rslt += jumptab(1, 2) + "OrdererType: " + typeorderer
    rslt += jumptab(1, 2) + "Addresses:"
    rslt += jumptab(1, 4) + "- " + "orderer0." + host + ".com:7050"
    rslt += jumptab(1, 2) + "BatchTimeout: 2s"
    rslt += jumptab(1, 2) + "BatchSize:"
    rslt += jumptab(1, 4) + "MaxMessageCount: " + messageCount
    rslt += jumptab(1, 4) + "AbsoluteMaxBytes: " + absoluteMaxBytes
    rslt += jumptab(1, 4) + "PreferredMaxBytes: " + preferredMaxBytes
    rslt += jumptab(1, 2) + "Organizations:"
    rslt += createPolicies()
    rslt = rslt + jumptab(1, 4) + "BlockValidation:"
    rslt = rslt + jumptab(1, 6) + "Type: ImplicitMeta"
    rslt = rslt + jumptab(1, 6) + 'Rule: \"ANY Writers\"'
    return (rslt)

#Part of configtx.yaml
def setOrg(tab):
    rslt = jumptab(1, 0) + createOrganisations("OrdererOrg", 0, tab[0])
    i = 2
    while (i < len(tab)):
        rslt += jumptab(2, 0) + createOrganisations(tab[i], 1, tab[0])
        i += 2
    return (rslt)

def createCapabilities():
    rslt = jumptab(2, 0) + "Capabilities:"
    rslt += jumptab(1, 2) + "Channel: &ChannelCapabilities"
    rslt += jumptab(1, 4) + "V1_4_3: true"
    rslt += jumptab(1, 4) + "V1_3: false"
    rslt += jumptab(1, 4) + "V1_1: false"
    rslt += jumptab(1, 2) + "Orderer: &OrdererCapabilities"
    rslt += jumptab(1, 4) + "V1_4_2: true"
    rslt += jumptab(1, 4) + "V1_1: false"
    rslt += jumptab(1, 2) + "Application: &ApplicationCapabilities"
    rslt += jumptab(1, 4) + "V1_4_2: true"
    rslt += jumptab(1, 4) + "V1_3: false"
    rslt += jumptab(1, 4) + "V1_2: false"
    rslt += jumptab(1, 4) + "V1_1: false"
    return rslt

def createPolicies():
    rslt = jumptab(1, 2) + "Policies:"
    rslt = rslt + jumptab(1, 4) + "Readers:"
    rslt = rslt + jumptab(1, 6) + "Type: ImplicitMeta"
    rslt = rslt + jumptab(1, 6) + 'Rule: \"ANY Readers\"'
    rslt = rslt + jumptab(1, 4) + "Writers:"
    rslt = rslt + jumptab(1, 6) + "Type: ImplicitMeta"
    rslt = rslt + jumptab(1, 6) + 'Rule: \"ANY Writers\"'
    rslt = rslt + jumptab(1, 4) + "Admins:"
    rslt = rslt + jumptab(1, 6) + "Type: ImplicitMeta"
    rslt = rslt + jumptab(1, 6) + 'Rule: \"MAJORITY Admins\"'
    return rslt

def createApplication():
    rslt = jumptab(2, 0) + "Application: &ApplicationDefaults"
    rslt += jumptab(1, 2) + "Organizations:"
    rslt += createPolicies()
    rslt += jumptab(1, 2) + "Capabilities:"
    rslt += jumptab(1, 4) + "<<: *ApplicationCapabilities"
    return rslt

def createChannelDefaults():
    rslt = jumptab(2, 0) + "Channel: &ChannelDefaults"
    rslt += createPolicies()
    rslt += jumptab(1, 2) + "Capabilities:"
    rslt += jumptab(1, 4) + "<<: *ChannelCapabilities"
    return rslt

#Call functions in order to create crypto-config.yaml
def createCryptoconfig(tab):
    buffer = ordererOrgConfig(tab[0])
    buffer += jumptab(2, 0) + "PeerOrgs:"
    i = 2
    while (i < len(tab)):
        buffer += jumptab(2, 1) + "- Name: " + tab[i].capitalize()
        buffer += jumptab(1, 1) + "  Domain: " + tab[i] + "." + tab[0] + ".com"
        buffer += jumptab(1, 1) + "  EnableNodeOUs: true"
        buffer += jumptab(2, 2) + "Template:"
        buffer += jumptab(1, 3) + "Count: " + tab[i + 1]
        buffer += jumptab(2, 2) + "Users:"
        buffer += jumptab(1, 3) + "Count: 3"
        i += 2
    return (buffer)

#Part of crypto-config.yaml - OrdererOrgs section
def ordererOrgConfig(host):
    rslt = "OrdererOrgs:"
    rslt += jumptab(2, 1) + "- Name: Orderer"
    rslt += jumptab(1, 1) + "  Domain: " + host + ".com"
    rslt += jumptab(1, 1) + "  EnableNodeOUs: true"
    rslt += jumptab(2, 2) + "Specs:"
    rslt += jumptab(1, 3) + "- Hostname: orderer0"
    return (rslt)

def createSoloConfigtx(tabName):
    buffer = "Organizations:"
    buffer += setOrg(tabName)
    buffer += createCapabilities()
    buffer += createApplication()
    buffer += createSoloOrderer("solo", "10", "98 MB", "512 KB", tabName[0])
    buffer += createChannelDefaults()
    buffer += jumptab(2, 0) + "Profiles:"
    buffer += jumptab(1, 0) + createProfiles(tabName, "MultiOrgsOrdererGenesis", "OrdererOrg")
    buffer += jumptab(1, 0) + createChannelProfile(tabName, "MultiOrgsChannel")
    return (buffer)

#Ask user for network mapping
def getArg():
    from environment.conf import Configuration
    master = []
    master.append(str(Configuration.returnNetworkName()))
    master.append(str(Configuration.returnChannelName()))
    orgCnt = Configuration.returnOrgCnt()
    os.system("./init.sh")
    os.system("mkdir templates")
    for i in range(orgCnt):
        orgNm = str(Configuration.returnOrgName(i))
        master.append(orgNm)
        os.system("mkdir " + orgNm)
        peerCnt = Configuration.returnPeerCnt(i)
        master.append(peerCnt)
        #os.system("mv " + orgNm + " ."+orgNm)
    return master

#Get number of peer
def getNumber():
    peerNb = -1
    while peerNb < 0:
        while True:
            try:
                peerNb = int(input("Number of peer : "))
                break
            except:
                print("Please enter a number")
        if peerNb < 0:
            print("Please enter a positive number")
    return (peerNb)

#Error handling
def sameName(tab, name):
    i = 2
    if name == "":
        return (0)
    while i < len(tab):
        if tab[i] == name:
            return (0)
        i += 2
    return (1)

#Docker-compose.yaml header
def headerDockerFile(network, hostname, swarm, isVol):
    rslt = "version: '3.4'"
    if(isVol):
        rslt += jumptab(2,0) + "volumes:"
        rslt += jumptab(1, 1) + hostname + ":"
    rslt += jumptab(1, 0) + "networks:"
    rslt += jumptab(1, 2) + network + ":"
    rslt += jumptab(1, 4) + "external:"
    rslt += jumptab(1, 6) + "name: " + swarm
    rslt += jumptab(2, 0) + "services:"
    return (rslt)

#Part of docker-compose.yaml - CA section
def caDockerFile(arch, hostname, rank, network, caHostname, port, inc):
    path = str(Configuration.returnNodePath(rank))
    rslt = jumptab(2, 1) + "org" + str(rank+1) + "ca:"
    rslt += jumptab(1, 2) + "image: hyperledger/fabric-ca:" + imageTag
    rslt += jumptab(1, 2) + "environment:"
    rslt += jumptab(1, 3) + "- FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server"
    rslt += jumptab(1, 3) + "- FABRIC_CA_SERVER_CA_NAME=" + caHostname
    rslt += jumptab(1, 3) + "- FABRIC_CA_SERVER_TLS_ENABLED=true"
    rslt += jumptab(1, 3) + "- FABRIC_CA_SERVER_CA_CERTFILE=/etc/hyperledger/fabric-ca-server-config/ca.org" + str(rank+1) + ".example.com-cert.pem"
    rslt += jumptab(1, 3) + "- FABRIC_CA_SERVER_TLS_CERTFILE=/etc/hyperledger/fabric-ca-server-config/ca.org" + str(rank+1) + ".example.com-cert.pem"
    rslt += jumptab(1, 3) + "- FABRIC_CA_SERVER_CA_KEYFILE=/etc/hyperledger/fabric-ca-server-config/CA" + str(rank+1) + "_PRIVATE_KEY"
    rslt += jumptab(1, 3) + "- FABRIC_CA_SERVER_TLS_KEYFILE=/etc/hyperledger/fabric-ca-server-config/CA" + str(rank+1) + "_PRIVATE_KEY"
    rslt += jumptab(1, 2) + "command: sh -c 'fabric-ca-server start --ca.certfile /etc/hyperledger/fabric-ca-server-config/ca." + hostname + "-cert.pem --ca.keyfile /etc/hyperledger/fabric-ca-server-config/*_sk -b admin:adminpw -d'"
    rslt += jumptab(1, 2) + "volumes:"
    rslt += jumptab(1, 3) + "- " + path + "/crypto-config/peerOrganizations/" + hostname + "/ca/:/etc/hyperledger/fabric-ca-server-config"
    rslt += jumptab(1, 2) + dockerDeploy(network, caHostname, rank+1, rank, port, inc)
    return (rslt)

#Useful function for create docker-compose.yaml
def container_name(value):
    return (jumptab(1, 2) + "container_name: " + value)

#Useful function for create docker-compose.yaml
def image(value):
    return (jumptab(1, 2) + "image: " + value)

#Useful function for create docker-compose.yaml
def working_dir(value):
    return (jumptab(1, 2) + "working_dir: " + value)

#Useful function for create docker-compose.yaml
def command(value):
    return (jumptab(1, 2) + "command: " + value)

def conf(value1, value2):
    return (jumptab(1, 2) + value1 + ": " + value2)

#Useful function for create docker-compose.yaml
def list_value(value):
    return (jumptab(1, 3) + "- " + value)

#Part of docker-compose.yaml - Zookeeper section
def zookeeperDockerFile(network, rank):
    rslt = jumptab(2, 1) + "zookeeper" + str(rank) + ":"
    rslt += container_name("zookeeper" + str(rank))
    rslt += jumptab(1, 2) + "extends:"
    rslt += jumptab(1, 4) + "file: docker-compose-base.yml"
    rslt += jumptab(1, 4) + "service: zookeeper"
    rslt += jumptab(1, 2) + "environment:"
    rslt += list_value("ZOO_MY_ID=" + str((rank + 1)))
    rslt += list_value("ZOO_SERVERS=server.1=zookeeper0:2888:3888 server.2=zookeeper1:2888:3888 server.3=zookeeper2:2888:3888")
    rslt += jumptab(1, 2) + "networks:"
    rslt += list_value(network)
    return (rslt)

#Part of docker-compose.yaml - Kafka section
def kafkaDockerFile(network, rank):
    rslt = jumptab(2, 1) + "kafka" + str(rank) + ":"
    rslt += container_name("kafka" + str(rank))
    rslt += jumptab(1, 2) + "extends:"
    rslt += jumptab(1, 4) + "file: docker-compose-base.yml"
    rslt += jumptab(1, 4) + "service: kafka"
    rslt += jumptab(1, 2) + "environment:"
    rslt += list_value("KAFKA_BROKER_ID=" + str(rank))
    rslt += list_value("KAFKA_ZOOKEEPER_CONNECT=zookeeper0:2181,zookeeper1:2181,zookeeper2:2181")
    rslt += list_value("KAFKA_MESSAGE_MAX_BYTES=103809024")
    rslt += list_value("KAFKA_REPLICA_FETCH_MAX_BYTES=103809024")
    rslt += list_value("KAFKA_REPLICA_FETCH_RESPONSE_MAX_BYTES=103809024")
    rslt += jumptab(1, 2) + "depends_on:"
    rslt += list_value("zookeeper0")
    rslt += list_value("zookeeper1")
    rslt += list_value("zookeeper2")
    rslt += jumptab(1, 2) + "networks:"
    rslt += list_value(network)
    return (rslt)

#Part of docker-compose.yaml - Orderer section
# def ordererDockerFile(hostname, rank, network, arch):
#     rslt = jumptab(2, 1) + "orderer." + hostname + ":"
#     rslt += container_name("orderer." + hostname)
#     rslt += image("hyperledger/fabric-orderer:" + imageTag)
#     rslt += working_dir("/opt/gopath/src/github.com/hyperledger/fabric")
#     rslt += command("orderer")
#     rslt += jumptab(1, 2) + "environment:"
#     rslt += list_value("CONFIGTX_ORDERER_ORDERERTYPE=kafka")
#     rslt += list_value("CONFIGTX_ORDERER_KAFKA_BROKERS=[kafka0:9092,kafka1:9092,kafka2:9092,kafka3:9092]")
#     rslt += list_value("ORDERER_KAFKA_RETRY_SHORTINTERVAL=1s")
#     rslt += list_value("ORDERER_KAFKA_RETRY_SHORTTOTAL=30s")
#     rslt += list_value("ORDERER_KAFKA_VERBOSE=true")
#     rslt += list_value("ORDERER_GENERAL_LOGLEVEL=debug")
#     rslt += list_value("ORDERER_GENERAL_LISTENADDRESS=0.0.0.0")
#     rslt += list_value("ORDERER_GENERAL_GENESISMETHOD=file")
#     rslt += list_value("ORDERER_GENERAL_GENESISFILE=/etc/hyperledger/configtx/genesis.block")
#     rslt += list_value("ORDERER_GENERAL_LOCALMSPID=OrdererMSP")
#     rslt += list_value("ORDERER_GENERAL_LOCALMSPDIR=/etc/hyperledger/msp/orderer/msp")
#     rslt += jumptab(1, 2) + "ports:"
#     rslt += list_value(str((7 + rank)) + "050:7050")
#     rslt += jumptab(1, 2) + "volumes:"
#     rslt += list_value("./channel-artifacts:/etc/hyperledger/configtx")
#     rslt += list_value("./crypto-config/ordererOrganizations/" + hostname + "/orderers/" + "orderer." + hostname +  "/msp:/etc/hyperledger/msp/orderer/msp")
#     rslt += jumptab(1, 2) + "depends_on:"
#     rslt += list_value("kafka0")
#     rslt += list_value("kafka1")
#     rslt += list_value("kafka2")
#     rslt += list_value("kafka3")
#     rslt += jumptab(1, 2) + "networks:"
#     rslt += list_value(network)
#     return (rslt)

def ordererSoloDockerFile(hostname, rank, network, arch):
    path = str(Configuration.returnNodePath(0))
    rslt = jumptab(2, 1) + "orderer0:"
    rslt += image("hyperledger/fabric-orderer:" + imageTag)
    rslt += working_dir("/opt/gopath/src/github.com/hyperledger/fabric")
    rslt += command("orderer")
    rslt += jumptab(1, 2) + "environment:"
    rslt += list_value("ORDERER_GENERAL_TLS_ENABLED=true")
    rslt += list_value("ORDERER_GENERAL_TLS_PRIVATEKEY=/var/hyperledger/tls/orderer/tls/server.key")
    rslt += list_value("ORDERER_GENERAL_TLS_CERTIFICATE=/var/hyperledger/tls/orderer/tls/server.crt")
    rslt += list_value("ORDERER_GENERAL_TLS_ROOTCAS=[/var/hyperledger/tls/orderer/tls/ca.crt]")
    rslt += list_value("ORDERER_KAFKA_RETRY_SHORTINTERVAL=1s")
    rslt += list_value("ORDERER_KAFKA_RETRY_SHORTTOTAL=30s")
    rslt += list_value("ORDERER_KAFKA_VERBOSE=true")
    rslt += list_value("ORDERER_GENERAL_LOGLEVEL=debug")
    rslt += list_value("ORDERER_GENERAL_LISTENADDRESS=0.0.0.0")
    rslt += list_value("ORDERER_GENERAL_GENESISMETHOD=file")
    rslt += list_value("ORDERER_GENERAL_GENESISFILE=/var/hyperledger/orderer/orderer.genesis.block")
    rslt += list_value("ORDERER_GENERAL_LOCALMSPID=OrdererMSP")
    rslt += list_value("ORDERER_GENERAL_LOCALMSPDIR=/var/hyperledger/msp/orderer/msp")
    rslt += list_value("ORDERER_GENERAL_CLUSTER_CLIENTCERTIFICATE=/var/hyperledger/orderer/tls/server.crt")
    rslt += list_value("ORDERER_GENERAL_CLUSTER_CLIENTPRIVATEKEY=/var/hyperledger/orderer/tls/server.key")
    rslt += list_value("ORDERER_GENERAL_CLUSTER_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]")
    rslt += jumptab(1, 2) + "volumes:"
    rslt += list_value(path + "/channel-artifacts/genesis.block:/var/hyperledger/orderer/orderer.genesis.block")
    rslt += list_value(path + "/crypto-config/ordererOrganizations/" + hostname + "/orderers/" + "orderer0." + hostname +  "/msp:/var/hyperledger/msp/orderer/msp")
    rslt += list_value(path + "/crypto-config/ordererOrganizations/" + hostname + "/orderers/" + "orderer0." + hostname +  "/tls:/var/hyperledger/tls/orderer/tls")
    rslt += list_value("orderer0.example.com:/var/hyperledger/production/orderer")
    hostname = "orderer0."+hostname
    rslt += jumptab(1, 2) + dockerDeploy(network, hostname, 1, 0, 7050, 0)
    return (rslt)

#Part of docker-compose.yaml - Coucdh section
def couchDBDockerFile(arch, network, hostname, rank, org, peer, port, inc):
    rslt = jumptab(2, 1) + "org" + str(org) + "couchdb" + str(peer) + ":"
    rslt += image("hyperledger/fabric-couchdb:" + imageTag)
    rslt += jumptab(1, 2) + "environment:"
    rslt += jumptab(1, 3) + "- COUCHDB_USER=couchdb"
    rslt += jumptab(1, 3) + "- COUCHDB_PASSWORD=couchdb123"
    rslt += jumptab(1, 2) + dockerDeploy(network, hostname, org, rank, port, inc)
    return rslt
    

def dockerDeploy(network, hostname, org, rank, port, inc):
    host = Configuration.returnNodeHostname(org-1)
    rslt = jumptab(0, 0) + "deploy:"
    rslt += jumptab(1, 4) + "mode: replicated"
    rslt += jumptab(1, 4) + "replicas: 1"
    rslt += jumptab(1, 4) + "restart_policy:"
    rslt += jumptab(1, 6) + "condition: on-failure"
    rslt += jumptab(1, 4) + "placement:"
    rslt += jumptab(1, 6) + "constraints:"
    rslt += jumptab(1, 8) + "- node.hostname == " + host
    rslt += jumptab(1, 2) + "ports:"
    rslt += jumptab(1, 4) + "- published: " + str(rank*inc + port)
    rslt += jumptab(1, 4) + "  target: " + str(port)
    rslt += jumptab(1, 4) + "  mode: host"
    if("peer" in hostname):
        rslt += jumptab(1, 4) + "- published: " + str(rank*inc + port+2)
        rslt += jumptab(1, 4) + "  target: " + str(port+2)
        rslt += jumptab(1, 4) + "  mode: host"
    rslt += jumptab(1, 2) + "networks:"
    rslt += jumptab(1, 4) + network + ":"
    rslt += jumptab(1, 6) + "aliases:"
    rslt += jumptab(1, 8) + "- " + hostname
    return (rslt)
      
# Part of docker-compose.yaml - Peer section
# def peerDockerFile(hostname, rank, network, arch, idd, name):
#     rslt = jumptab(2, 1) + "peer" + str(idd) + "." + hostname + ":"
#     rslt += image("hyperledger/fabric-peer:" + imageTag)
#     rslt += working_dir("/opt/gopath/src/github.com/hyperledger/fabric")
#     rslt += command("peer node start")
#     rslt += jumptab(1, 2) + "environment:"
#     rslt += list_value("CORE_LOGGING_LEVEL=debug")
#     rslt += list_value("CORE_CHAINCODE_LOGGING_LEVEL=DEBUG")
#     rslt += list_value("CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock")
#     rslt += list_value("CORE_PEER_ID=peer" + str(idd) + "." + hostname)
#     rslt += list_value("CORE_PEER_ADDRESS=peer" + str(idd) + "." + hostname + ":7051")
#     rslt += list_value("CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=$NETWORK_NAME")
#     rslt += list_value("CORE_PEER_LOCALMSPID=" + name + "MSP")
#     rslt += list_value("CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/peer/msp")d
#     rslt += list_value("CORE_LEDGER_STATE_STATEDATABASE=CouchDB")
#     rslt += list_value("CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdb" + str(rank) + ":5984")
#     rslt += jumptab(1, 2) + "volumes:"
#     rslt += list_value("/var/run/:/host/var/run/")
#     rslt += list_value("./crypto-config/peerOrganizations/" + hostname + "/peers/peer" + str(idd) + "." + hostname + "/msp:/etc/hyperledger/peer/msp")
#     rslt += list_value("./crypto-config/peerOrganizations/" + hostname + "/users:/etc/hyperledger/msp/users")
#     rslt += jumptab(1, 2) + "networks:"
#     rslt += jumptab(1, 2) + dockerDeploy(network, hostname, org, rank, port, inc)
#     return (rslt)

def peerTLSDockerFile(hostname, rank, network, peerHostname, couchHostname,idd, name, org, port, inc, swarm):
    path = str(Configuration.returnNodePath(org-1))
    rslt = jumptab(2, 1) + "org" + str(org) + "peer" + str(idd) +  ":"
    rslt += image("hyperledger/fabric-peer:" + imageTag)
    rslt += working_dir("/opt/gopath/src/github.com/hyperledger/fabric/peer")
    rslt += command("peer node start")
    rslt += jumptab(1, 2) + "environment:"
    rslt += list_value("CORE_LEDGER_STATE_STATEDATABASE=CouchDB")
    rslt += list_value("CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=" + couchHostname + ":5984")
    rslt += list_value("CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=couchdb")
    rslt += list_value("CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=couchdb123")
    rslt += list_value("CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock")
    rslt += list_value("CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE="+swarm)
    rslt += list_value("CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:7052")
    rslt += list_value("CORE_LOGGING_LEVEL=DEBUG")
    rslt += list_value("CORE_PEER_TLS_ENABLED=true")
    rslt += list_value("CORE_PEER_GOSSIP_USELEADERELECTION=true")
    rslt += list_value("CORE_PEER_GOSSIP_ORGLEADER=false")
    rslt += list_value("CORE_PEER_PROFILE_ENABLED=true")
    rslt += list_value("CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt")
    rslt += list_value("CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key")
    rslt += list_value("CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt")
    rslt += list_value("CORE_PEER_ID=" + peerHostname)
    rslt += list_value("CORE_PEER_ADDRESS=" + peerHostname + ":" + str(rank*inc + port))
    rslt += list_value("CORE_PEER_GOSSIP_BOOTSTRAP=peer0.org" + str(org) + ".example.com:7051")    
    rslt += list_value("CORE_PEER_GOSSIP_EXTERNALENDPOINT=" + peerHostname + ":" + str(rank*inc + port))
    rslt += list_value("CORE_PEER_LOCALMSPID=Org" + str(org) + "MSP")
    rslt += list_value("CORE_VM_DOCKER_ATTACHSTDOUT=true")
    rslt += list_value("CORE_CHAINCODE_STARTUPTIMEOUT=1200s")
    rslt += list_value("CORE_CHAINCODE_EXECUTETIMEOUT=800s")
    rslt += jumptab(1, 2) + "volumes:"
    rslt += list_value("/var/run/:/host/var/run/")
    rslt += list_value(path + "/crypto-config/peerOrganizations/" + hostname + "/peers/peer" + str(idd) + "." + hostname + "/msp:/etc/hyperledger/fabric/msp")
    rslt += list_value(path + "/crypto-config/peerOrganizations/" + hostname + "/peers/peer" + str(idd) + "." + hostname + "/tls:/etc/hyperledger/fabric/tls")
    rslt += list_value(peerHostname + ":/var/hyperledger/production")
    rslt += jumptab(1, 2) + dockerDeploy(network, peerHostname, org, rank, port, inc)
    return (rslt)

def cliDockerFile(hostname,network, arch ,name, org):
    path = str(Configuration.returnNodePath(org-1))
    host = Configuration.returnNodeHostname(org-1)
    rslt = jumptab(2, 1) + name + "cli" + ":"
    rslt += image("hyperledger/fabric-tools:" + imageTag)
    rslt += conf("tty","true")
    rslt += conf("stdin_open","true")
    rslt += jumptab(1, 2) + "deploy:"
    rslt += jumptab(1, 4) + "mode: replicated"
    rslt += jumptab(1, 4) + "replicas: 1"
    rslt += jumptab(1, 4) + "restart_policy:"
    rslt += jumptab(1, 6) + "condition: on-failure"
    rslt += jumptab(1, 4) + "placement:"
    rslt += jumptab(1, 6) + "constraints:"
    rslt += jumptab(1, 8) + "- node.hostname == " + host
    rslt += working_dir("/opt/gopath/src/github.com/hyperledger/fabric/peer")
    rslt += command("/bin/bash")
    rslt += jumptab(1, 2) + "environment:"
    rslt += list_value("GOPATH=/opt/gopath")
    rslt += list_value("CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock")
    rslt += list_value("CORE_PEER_ID=cli" + name)
    rslt += list_value("CORE_PEER_LOCALMSPID=Org" + str(org) + "MSP")
    rslt += list_value("CORE_PEER_ADDRESS=peer0." + hostname + ":7051")
    rslt += list_value("CORE_PEER_TLS_ENABLED=true")
    rslt += list_value("CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/" + hostname + "/peers/peer0." + hostname + "/tls/server.crt")
    rslt += list_value("CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/" + hostname + "/peers/peer0." + hostname + "/tls/server.key")
    rslt += list_value("CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/" + hostname + "/peers/peer0." + hostname + "/tls/ca.crt")
    rslt += list_value("CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/" + hostname + "/users/Admin@" + hostname + "/msp")
    rslt += jumptab(1, 2) + "volumes:"
    rslt += list_value("/var/run/:/host/var/run/")
    rslt += list_value(path + "/crypto-config:/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/")
    rslt += list_value(path+ "/channel-artifacts:/opt/gopath/src/github.com/hyperledger/fabric/peer/channel-artifacts")
    rslt += list_value("/tmp/multi/env.sh:/opt/gopath/src/github.com/hyperledger/fabric/peer/env.sh")
    if(org == 1):
        rslt += list_value(path + "/cli_scripts/:/opt/gopath/src/github.com/hyperledger/fabric/peer/scripts")
        rslt += list_value(path + "/chaincode/:/opt/gopath/src/github.com/chaincode")
        rslt += list_value(path + "/py_metrics:/opt/gopath/src/github.com/hyperledger/fabric/peer/metrics/")
        rslt += list_value(path + "/results:/opt/gopath/src/github.com/hyperledger/fabric/peer/results")
    rslt += jumptab(1, 2) + "networks:"
    rslt += list_value(network)
    return (rslt)

# #Call functions in order to create docker-composer.yaml
# def createDockerFile(tab):
#     network = tab[0]
#     arch = tab[1]
#     orgNB = int((len(tab) - 2) / 2)
#     buffer = headerDockerFile(network)
#     buffer += ordererDockerFile(network + ".com", 0, network, "$ARCH")
#     for i in range (0, 4):
#         buffer += kafkaDockerFile(network, i)
#     for i in range (0, 3):
#         buffer += zookeeperDockerFile(network, i)
#     rank = 0
#     index = 3
#     for k in range (0, orgNB):
#         buffer += caDockerFile("$ARCH", tab[index - 1] + "." + network + ".com", k, network)
#         for i in range (0, int(tab[index])):
#             buffer += peerDockerFile(tab[index - 1] + "." + network + ".com", rank, network, "$ARCH", i, tab[index - 1])
#             buffer += couchDBDockerFile("$ARCH", network, rank)
#             rank += 1
#         index += 2
#     return (buffer)

def writeToFile(fileName, data):
    file = open(fileName, "w")
    file.write(data)
    file.close()

def createSoloDockerFile(tab):
    network = str(Configuration.returnNetworkName())
    swarm = str(Configuration.returnStackName())
    orgNB = Configuration.returnOrgCnt()
    name = "orderer0"
    hostname = name + "." + network + ".com"
    buffer = headerDockerFile(network, hostname, swarm, True)
    buffer += ordererSoloDockerFile(network + ".com", 0, network, "$ARCH")
    fileName = "docker-compose-orderer.yaml"
    writeToFile(fileName, buffer)
    rank = 0
    index = 3
    for k in range (0, orgNB):
        orgNm = str(Configuration.returnOrgName(k))
        hostname = "ca." + orgNm + "." + network + ".com"
        buffer = headerDockerFile(network, hostname, swarm, True)
        buffer += caDockerFile("$ARCH", orgNm + "." + network + ".com", k, network, hostname, 7054, 0)
        fileName = "templates/docker-compose-ca" + str(int(k+1)) + "-template.yaml"
        writeToFile(fileName, buffer)
        peerCnt = int(Configuration.returnPeerCnt(k))
        for i in range (0, peerCnt):
            peerHostname = "peer" + str(i) + "." + orgNm + "." + network + ".com"
            couchHostname = "couchdb" + str(i) + "." + orgNm + "." + network + ".com"
            buffer = headerDockerFile(network, peerHostname, swarm, True)
            buffer += couchDBDockerFile("$ARCH", network, couchHostname, rank, k+1, i, 5984, 2)
            buffer += peerTLSDockerFile(orgNm + "." + network + ".com", rank, network, peerHostname, couchHostname,i, orgNm, k+1, 7051, 1000, swarm)
            fileName = orgNm + "/docker-compose-org" + str(int(k+1)) + "-peer" + str(i) + ".yaml"
            writeToFile(fileName, buffer)
            rank += 1
        buffer = headerDockerFile(network, hostname, swarm, False)
        buffer += cliDockerFile(orgNm + "." + network + ".com",network, "$ARCH", orgNm, k+1)
        fileName = orgNm + "/docker-compose-org" + str(int(k+1)) + "-cli.yaml"
        writeToFile(fileName, buffer)
        index += 2
        rank = 0
    return (buffer)

#Part of launch.sh
def createGenNeeded(channelId, tab):
    orgNB = int((len(tab) - 2) / 2)
    index = 2
    rslt = "function gen_needed(){\nmkdir channel-artifacts\nexport CHANNEL_NAME=" + channelId + "\nexport FABRIC_CFG_PATH=$PWD\n./cryptogen generate --config=./crypto-config.yaml\n./configtxgen -profile ProfileTest -outputBlock ./channel-artifacts/genesis.block\n"
    rslt += "./configtxgen -profile ChannelTest -outputCreateChannelTx ./channel-artifacts/channel.tx   -channelID " + channelId
    for i in range (0, orgNB):
        rslt += jumptab(1, 0) + "./configtxgen -profile ChannelTest -outputAnchorPeersUpdate ./channel-artifacts/" + tab[index] + "MSPanchors.tx -channelID " + channelId + " -asOrg " + tab[index]
        index += 2
    rslt += "\n"
    rslt += "}\n"
    return (rslt)

#Part of launch.sh
def createConst():
    rslt = "function clean_it() {\ndocker rm -f $(docker ps -a -q)\nrm -rf crypto-config/*\nrm -rf channel-artifacts/*\n}\n\nfunction start_network() {\ndocker-compose -f docker-compose.yml up -d && docker ps\n}\n\nclean_it\ngen_needed\nstart_network\nsleep 20\njoin_channel"
    return (rslt)

#Part of launch.sh
def createJoinChannel(tab, channelId):
    network = tab[0]
    index = 3
    orgNB = int((len(tab) - 2) / 2)
    hostname=tab[index - 1] + "." + tab[0] + ".com"
    rslt = "function join_channel() {\ndocker exec -e CORE_PEER_LOCALMSPID=" + tab[index - 1] + "MSP -e CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@" + hostname + "/msp peer0." + tab[index - 1] + "." + tab[0] + ".com peer channel create -o orderer." + tab[0] + ".com:7050 -c " + channelId + " -f /etc/hyperledger/configtx/channel.tx --tls true --cafile /etc/hyperledger/msp/orderer/msp/tlscacerts/tlsca." + network + ".com-cert.pem"
    rslt += jumptab(1, 0) + "docker exec peer0." + tab[2] + "." + tab[0] + ".com cp " + channelId + ".block /etc/hyperledger/configtx"
    for i in range (0, orgNB):
        hostname=tab[index - 1] + "." + tab[0] + ".com"
        for k in range (0, int(tab[index])):
            rslt += jumptab(1, 0) + "docker exec peer" + str(k) + "." + tab[index - 1] + "." + tab[0] + ".com peer channel join -b /etc/hyperledger/configtx/" + channelId + ".block"
        rslt += jumptab(1, 0) + "docker exec -e CORE_PEER_LOCALMSPID=" + tab[index - 1] + "MSP -e CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@" + hostname + "/msp peer0." + tab[index - 1] + "." + tab[0] + ".com peer channel update -o orderer." + network + ".com:7050 -c " + channelId + " -f /etc/hyperledger/configtx/" + tab[index - 1] +"MSPanchors.tx --tls true --cafile /etc/hyperledger/msp/orderer/msp/tlscacerts/tlsca." + network + ".com-cert.pem" 
        index += 2
    rslt += "\n}"
    return (rslt)

#Call functions in order to create launch.sh
def createScript(tab):
    buffer = "#!/bin/bash"
    buffer += jumptab(2, 0) + createGenNeeded(tab[1], tab)
    buffer += jumptab(2, 0) + createJoinChannel(tab, tab[1])
    buffer += jumptab(2, 0) + createConst()
    return (buffer)

#Main function
def createNewOrg():
    tab = getArg()
    cryptoConfig = open("crypto-config.yaml", "w")
    configtx = open("configtx.yaml", "w")
    composeBuffer = createSoloDockerFile(tab)
    cryptoBuffer = createCryptoconfig(tab)
    configtxBuffer = createSoloConfigtx(tab)
    cryptoConfig.write(cryptoBuffer)
    cryptoConfig.close()
    configtx.write(configtxBuffer)
    configtx.close()


createNewOrg()

