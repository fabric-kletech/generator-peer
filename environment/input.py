import json
class Input(object):

    @staticmethod
    def getUserInput():
        data={}
        data['Net_name'] = input("Enter the name of the network: ")
        data['Channel_name'] = input("Enter the name of first channel: ")
        data['Stack'] = input("Enter the name of Docker Stack: ")
        j = int(input("Enter the number of orgs you want: "))
        data['Orgs'] = []
        data['Nodes'] = []
        for i in range(1,j+1):
            name = 'org' + str(i)
            peerCnt = input("Enter the number of peers for Org" + str(i)+": ")
            nodeIp = input("Enter the Ip of Node " + str(i)+": ")
            hostName = input("Enter the hostname for Node " + str(i) + ": ")
            path = input("Enter path for Node " + str(i) + ":")
            data['Orgs'].append({
                'name': str(name),
                'peerCnt':str(peerCnt)
            })
            data['Nodes'].append({
                'ip':str(nodeIp),
                'hostname':str(hostName),
                'path':str(path)
            })
        with open("conf.json","w") as json_dump:
            json.dump(data,json_dump,indent=4)