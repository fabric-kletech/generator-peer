import os
import subprocess
import sys
from conf import Configuration
class Shim(object):
    @staticmethod
    def setVar(name , val):
        os.environ[str(name)] = str(val) # visible in this process + all children
        
    
if __name__ == "__main__":
    Shim.setVar("NODE2_IP",Configuration.returnNodeIP(1))
    # os.system("echo $NODE2_IP") 