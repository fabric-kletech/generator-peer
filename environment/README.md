# Environment Generator
Our Task is to create a genrator script that generates(optional) <em>conf.json</em> which stores the configration information about the whole network. This <em>conf.js</em> will then be consumed by <em>conf.py</em> to create a python object that can be called by any other script.

A <em>shim.py</em> will also be created for offer a connectivity between bash script and python code. In addition to this a <em>env.py</em> will be used to set environment variables.